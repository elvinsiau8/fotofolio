INSERT INTO "payment_channels" ("channel_type", "name", "image_path", "status") VALUES
('VIRTUAL_ACCOUNT_BCA', 'BCA VIRTUAL ACCOUNT', 'https://cdn-doku.oss-ap-southeast-5.aliyuncs.com/doku-ui-framework/doku/img/bank/bca.png', true),
('VIRTUAL_ACCOUNT_BANK_MANDIRI', 'BANK MANDIRI VIRTUAL ACCOUNT', 'https://cdn-doku.oss-ap-southeast-5.aliyuncs.com/doku-ui-framework/doku/img/bank/mandiri.png', true),
('VIRTUAL_ACCOUNT_BANK_SYARIAH_MANDIRI', 'BANK SYARIAH MANDIRI VIRTUAL ACCOUNT', 'https://cdn-doku.oss-ap-southeast-5.aliyuncs.com/doku-ui-framework/doku/img/bank/mandirisyariah.png', true),
('VIRTUAL_ACCOUNT_DOKU', 'DOKU VIRTUAL ACCOUNT', 'https://cdn-doku.oss-ap-southeast-5.aliyuncs.com/doku-ui-framework/doku/img/bank/doku_va.png', true),
('VIRTUAL_ACCOUNT_BRI', 'BRI VIRTUAL ACCOUNT', 'https://cdn-doku.oss-ap-southeast-5.aliyuncs.com/doku-ui-framework/doku/img/bank/bri.png', true),
('VIRTUAL_ACCOUNT_BNI', 'BNI VIRTUAL ACCOUNT', 'https://cdn-doku.oss-ap-southeast-5.aliyuncs.com/doku-ui-framework/doku/img/bank/bni.png', true),
('VIRTUAL_ACCOUNT_BANK_PERMATA', 'BANK PERMATA VIRTUAL ACCOUNT', 'https://cdn-doku.oss-ap-southeast-5.aliyuncs.com/doku-ui-framework/doku/img/bank/permata.png', true),
('VIRTUAL_ACCOUNT_BANK_CIMB', 'BANK CIMB VIRTUAL ACCOUNT', 'https://cdn-doku.oss-ap-southeast-5.aliyuncs.com/doku-ui-framework/doku/img/bank/cimb.png', true),
('VIRTUAL_ACCOUNT_BANK_DANAMON', 'BANK DANAMON VIRTUAL ACCOUNT', 'https://cdn-doku.oss-ap-southeast-5.aliyuncs.com/doku-ui-framework/doku/img/bank/danamon.png', true);

INSERT INTO "categories" ("name") VALUES
('Music'),
('Sports'),
('Festival'),
('Workshop');