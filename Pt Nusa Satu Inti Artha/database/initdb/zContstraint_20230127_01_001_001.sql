ALTER TABLE ONLY "users"
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

ALTER TABLE ONLY "transactions"
    ADD CONSTRAINT transactions_pkey PRIMARY KEY (id);

ALTER TABLE ONLY "payment_channels"
    ADD CONSTRAINT payment_pkey PRIMARY KEY (id);
   
ALTER TABLE ONLY "transaction_tickets"
    ADD CONSTRAINT transaction_ticket_pkey PRIMARY KEY (id);
 
ALTER TABLE ONLY "merchants"
    ADD CONSTRAINT merchants_pkey PRIMARY KEY (id);
    
ALTER TABLE ONLY "categories"
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);
    
ALTER TABLE ONLY "events"
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);
    
ALTER TABLE ONLY "tickets"
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);

ALTER TABLE ONLY "tiers"
    ADD CONSTRAINT tier_pkey PRIMARY KEY (id);

ALTER TABLE ONLY "event_images"
    ADD CONSTRAINT event_image_pkey PRIMARY KEY (id);
   
ALTER TABLE ONLY "transactions"
    ADD CONSTRAINT transactions_users_fkey FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE ONLY "transactions"
    ADD CONSTRAINT transactions_payment_fkey FOREIGN KEY ("payment_id") REFERENCES "payment_channels" ("id");

ALTER TABLE ONLY "transaction_tickets"
    ADD CONSTRAINT transactions_transaction_ticket_fkey FOREIGN KEY ("transaction_id") REFERENCES "transactions" ("id");

ALTER TABLE ONLY "transaction_tickets"
    ADD CONSTRAINT transactions_tier_fkey FOREIGN KEY ("tier_id") REFERENCES "tiers" ("id");
    
ALTER TABLE ONLY "events"
    ADD CONSTRAINT merchant_id_fkey FOREIGN KEY ("merchant_id") REFERENCES "merchants"("id"),
    ADD CONSTRAINT category_id_fkey FOREIGN KEY ("category_id") REFERENCES "categories"("id");
    
ALTER TABLE ONLY "tickets"
    ADD CONSTRAINT event_id_fkey FOREIGN KEY ("event_id") REFERENCES "events"("id");

ALTER TABLE ONLY "tiers"
    ADD CONSTRAINT ticket_id_fkey FOREIGN KEY ("ticket_id") REFERENCES "tickets"("id");
    
ALTER TABLE ONLY "event_images"
    ADD CONSTRAINT event_id_fkey FOREIGN KEY ("event_id") REFERENCES "events"("id");