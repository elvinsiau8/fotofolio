CREATE TABLE "users" (
  "id" CHARACTER VARYING(50) NOT NULL,
  "email" CHARACTER VARYING(50) NOT NULL,
  "username" CHARACTER VARYING(50) NOT NULL,
  "password" CHARACTER VARYING(50) NOT NULL,
  "name" CHARACTER VARYING(30) NOT NULL,
  "dob" DATE NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE users IS 'Table for customer data';

CREATE TABLE "transactions" (
  "id" CHARACTER VARYING(50) NOT NULL,
  "user_id" CHARACTER VARYING(50) NOT NULL,
  "payment_id" INTEGER,
  "total_qty" INTEGER NOT NULL,
  "total_price" BIGINT NOT NULL,
  "payment_status" CHARACTER VARYING(15) NOT NULL,
  "purchased_date" TIMESTAMP WITH TIME ZONE,
  "request_id" CHARACTER VARYING(128),
  "invoice_id" CHARACTER VARYING(128),
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE transactions IS 'Table for transaction data';

CREATE TABLE "transaction_tickets" (
  "id" CHARACTER VARYING(100) NOT NULL,
  "transaction_id" CHARACTER VARYING(50) NOT NULL,
  "tier_id" INTEGER,
  "purchased_qty" INTEGER NOT NULL,
  "tier_total_price" BIGINT NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE transaction_tickets IS 'Table for transaction ticket data';


CREATE TABLE "payment_channels" (
  "id" SERIAL,
  "channel_type" CHARACTER VARYING(100) NOT NULL,
  "name" CHARACTER VARYING(50) NOT NULL,
  "image_path" CHARACTER VARYING(255),
  "status" BOOLEAN NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE payment_channels IS 'Table for payment channel data';

CREATE TABLE "merchants"(
  "id" CHARACTER VARYING(50) NOT NULL,
  "email" CHARACTER VARYING(50) NOT NULL,
  "username" CHARACTER VARYING(50) NOT NULL,
  "password" CHARACTER VARYING(50) NOT NULL,
  "name" CHARACTER VARYING(30) NOT NULL,
  "bio" TEXT,
  "location" CHARACTER VARYING(50) NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE merchants IS 'Table for merchants data';

CREATE TABLE "categories"(
  "id" SERIAL,
  "name" CHARACTER VARYING(30) NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE categories IS 'Table for categories data';

CREATE TABLE "events"(
  "id" SERIAL,
  "merchant_id" CHARACTER VARYING(50) NOT NULL,
  "category_id" INTEGER,
  "name" CHARACTER VARYING(50) NOT NULL,
  "date" DATE,
  "sell_start_date" DATE,
  "sell_end_date" DATE,
  "description" TEXT,
  "location" CHARACTER VARYING(50) NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE events IS 'Table for events data';

CREATE TABLE "tickets"(
  "id" SERIAL,
  "event_id" INTEGER NOT NULL,
  "name" CHARACTER VARYING(30) NOT NULL,
  "date" DATE,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE tickets IS 'Table for ticket data';

CREATE TABLE "tiers"(
  "id" SERIAL,
  "ticket_id" INTEGER NOT NULL,
  "name" CHARACTER VARYING(30) NOT NULL,
  "description" TEXT,
  "price" BIGINT NOT NULL,
  "qty" INTEGER NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE tiers IS 'Table for tier data';

CREATE TABLE "event_images"(
  "id" SERIAL,
  "event_id" INTEGER,
  "image_link" CHARACTER VARYING(50) NOT NULL,
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE event_images IS 'Table for image of event data';