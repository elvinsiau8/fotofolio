ALTER TABLE ONLY "users"
    ADD CONSTRAINT "user_username_unique" UNIQUE ("username"),
    ADD CONSTRAINT "user_email_unique" UNIQUE ("email");

ALTER TABLE ONLY "merchants"
    ADD CONSTRAINT "merchant_username_unique" UNIQUE ("username"),
    ADD CONSTRAINT "merchant_email_unique" UNIQUE ("email");

ALTER TABLE ONLY "transactions"
    ADD CONSTRAINT "request_id_unique" UNIQUE ("id"),
    ADD CONSTRAINT "invoice_id_unique" UNIQUE ("id");

ALTER TABLE ONLY "payment_channels"
    ADD CONSTRAINT "payment_channel_channel_type_unique" UNIQUE ("channel_type"),
    ADD CONSTRAINT "payment_channel_name_unique" UNIQUE ("name");