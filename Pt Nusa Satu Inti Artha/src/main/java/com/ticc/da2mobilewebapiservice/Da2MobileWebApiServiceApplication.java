package com.ticc.da2mobilewebapiservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Da2MobileWebApiServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(Da2MobileWebApiServiceApplication.class, args);
	}

}
