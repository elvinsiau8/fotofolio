package com.ticc.da2mobilewebapiservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ticc.da2mobilewebapiservice.entity.TransactionTicket;

@Repository
public interface TransactionTicketRepository extends JpaRepository<TransactionTicket,String>{
    
}
