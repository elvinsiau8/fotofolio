package com.ticc.da2mobilewebapiservice.repository;

import com.ticc.da2mobilewebapiservice.entity.Event;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    Optional<Event> findByIdAndDeletedAtIsNull(Long id);
}
