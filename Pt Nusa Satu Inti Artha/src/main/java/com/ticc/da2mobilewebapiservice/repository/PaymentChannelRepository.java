package com.ticc.da2mobilewebapiservice.repository;

import com.ticc.da2mobilewebapiservice.entity.PaymentChannel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentChannelRepository extends JpaRepository<PaymentChannel, Long> {
}
