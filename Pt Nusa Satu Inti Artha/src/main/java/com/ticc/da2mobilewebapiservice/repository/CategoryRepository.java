package com.ticc.da2mobilewebapiservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ticc.da2mobilewebapiservice.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
  