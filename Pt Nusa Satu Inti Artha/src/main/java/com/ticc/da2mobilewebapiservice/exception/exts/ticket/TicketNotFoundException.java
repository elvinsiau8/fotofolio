package com.ticc.da2mobilewebapiservice.exception.exts.ticket;

public class TicketNotFoundException extends RuntimeException {
    public TicketNotFoundException(String message) {
        super(message);
    }
}
