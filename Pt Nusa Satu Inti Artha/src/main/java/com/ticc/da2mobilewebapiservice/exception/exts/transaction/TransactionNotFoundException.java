package com.ticc.da2mobilewebapiservice.exception.exts.transaction;

public class TransactionNotFoundException extends RuntimeException {
    public TransactionNotFoundException(String message) {
        super(message);
    }
}
