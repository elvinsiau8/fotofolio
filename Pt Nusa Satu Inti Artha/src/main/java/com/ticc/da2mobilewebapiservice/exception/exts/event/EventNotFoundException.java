package com.ticc.da2mobilewebapiservice.exception.exts.event;

public class EventNotFoundException extends RuntimeException {
    public EventNotFoundException(String message) {
        super(message);
    }
}
