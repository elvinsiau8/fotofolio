package com.ticc.da2mobilewebapiservice.exception.exts.eventimage;

public class EventImageNotFoundException extends RuntimeException {
    public EventImageNotFoundException(String message) {
        super(message);
    }
}
