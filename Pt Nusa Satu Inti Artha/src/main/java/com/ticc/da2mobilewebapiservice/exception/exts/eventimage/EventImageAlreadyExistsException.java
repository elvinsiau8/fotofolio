package com.ticc.da2mobilewebapiservice.exception.exts.eventimage;

public class EventImageAlreadyExistsException extends RuntimeException {
    public EventImageAlreadyExistsException(String message) {
        super(message);
    }
}
