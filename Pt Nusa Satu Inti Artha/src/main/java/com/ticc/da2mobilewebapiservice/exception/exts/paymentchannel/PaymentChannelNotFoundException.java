package com.ticc.da2mobilewebapiservice.exception.exts.paymentchannel;

public class PaymentChannelNotFoundException extends RuntimeException {
    public PaymentChannelNotFoundException(String message) {
        super(message);
    }
}
