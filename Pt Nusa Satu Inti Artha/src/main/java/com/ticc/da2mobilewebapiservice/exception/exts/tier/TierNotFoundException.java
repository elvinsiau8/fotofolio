package com.ticc.da2mobilewebapiservice.exception.exts.tier;

public class TierNotFoundException extends RuntimeException {
    public TierNotFoundException(String message) {
        super(message);
    }
}
