package com.ticc.da2mobilewebapiservice.exception.exts.merchant;

public class MerchantAlreadyExistsException extends RuntimeException {
    public MerchantAlreadyExistsException(String message) {
        super(message);
    }
}
