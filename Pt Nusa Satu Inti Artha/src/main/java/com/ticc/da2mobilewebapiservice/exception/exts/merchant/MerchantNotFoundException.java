package com.ticc.da2mobilewebapiservice.exception.exts.merchant;

public class MerchantNotFoundException extends RuntimeException {
    public MerchantNotFoundException(String message) {
        super(message);
    }
}
