package com.ticc.da2mobilewebapiservice.exception.exts.merchant;

public class MerchantRequestNotValidException extends RuntimeException {
    public MerchantRequestNotValidException(String message) {
        super(message);
    }
}
