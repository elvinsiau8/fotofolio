package com.ticc.da2mobilewebapiservice.service;

import com.ticc.da2mobilewebapiservice.dto.response.MerchantResponseDTO;

public interface MerchantAuthService {
    String validateAndGenerated(String username,String password);

    MerchantResponseDTO decodeToken(String token);
}
