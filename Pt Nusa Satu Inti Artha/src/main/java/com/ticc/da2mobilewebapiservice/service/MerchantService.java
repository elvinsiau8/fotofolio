package com.ticc.da2mobilewebapiservice.service;

import java.util.List;

import com.ticc.da2mobilewebapiservice.dto.request.MerchantAddRequestDTO;
import com.ticc.da2mobilewebapiservice.dto.response.EventResponseDTO;
import com.ticc.da2mobilewebapiservice.dto.response.MerchantResponseDTO;

public interface MerchantService {
    MerchantResponseDTO addMerchant(MerchantAddRequestDTO merchantAddRequestDTO);
    MerchantResponseDTO getMerchantByUsername(String username);
    List<EventResponseDTO> getEventByMerchant(String username);
}
