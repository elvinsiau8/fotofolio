package com.ticc.da2mobilewebapiservice.service;

import com.ticc.da2mobilewebapiservice.dto.response.PaymentChannelResponseDTO;

import java.util.List;

public interface PaymentChannelService {
    List<PaymentChannelResponseDTO> getPaymentChannels();

    PaymentChannelResponseDTO updateStatus(Long id);
}
