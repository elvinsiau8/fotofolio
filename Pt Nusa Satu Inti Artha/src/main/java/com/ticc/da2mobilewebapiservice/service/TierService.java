package com.ticc.da2mobilewebapiservice.service;

import com.ticc.da2mobilewebapiservice.dto.request.TierRequestDTO;
import com.ticc.da2mobilewebapiservice.dto.response.TierResponseDTO;
import com.ticc.da2mobilewebapiservice.entity.Tier;

import java.util.Optional;

public interface TierService {
    TierResponseDTO addTier(TierRequestDTO tierRequestDTO);
    TierResponseDTO updateTier(TierRequestDTO tierRequestDTO, Long id);
    boolean delTier(Long id);
    Optional<Tier> findById(Long id);
}
