package com.ticc.da2mobilewebapiservice.service;

import com.ticc.da2mobilewebapiservice.dto.request.EventRequestDTO;
import com.ticc.da2mobilewebapiservice.dto.response.EventDetailResponseDTO;
import com.ticc.da2mobilewebapiservice.dto.response.EventResponseDTO;

public interface EventService {

    EventResponseDTO addEvent(EventRequestDTO eventRequestDTO);

    EventDetailResponseDTO findById(Long id);

    EventResponseDTO updateEvent(Long id, EventRequestDTO eventRequestDTO);

    boolean delEvent(Long id);
}
