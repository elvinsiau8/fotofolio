package com.ticc.da2mobilewebapiservice.service;

import com.ticc.da2mobilewebapiservice.dto.request.TicketRequestDTO;
import com.ticc.da2mobilewebapiservice.dto.response.TicketResponseDTO;
import com.ticc.da2mobilewebapiservice.dto.response.TierResponseDTO;
import com.ticc.da2mobilewebapiservice.entity.Ticket;

import java.util.List;
import java.util.Optional;

public interface TicketService {
    TicketResponseDTO addTicket(TicketRequestDTO ticketRequestDTO);

    TicketResponseDTO updateTicket(Long id, TicketRequestDTO ticketRequestDTO);

    boolean delTicket(Long id);

    List<TierResponseDTO> findTicketTier(Long id);

    Optional<Ticket> findById(Long id);
}
