package com.ticc.da2mobilewebapiservice.service;

import java.util.List;

import com.ticc.da2mobilewebapiservice.dto.request.EventImageRequestDTO;
import com.ticc.da2mobilewebapiservice.dto.response.EventImageResponseDTO;

public interface EventImageService {
    EventImageResponseDTO addEventImage(EventImageRequestDTO eventImageRequestDTO);
    List<EventImageResponseDTO> getEventImageByEventId(Long id);
    EventImageResponseDTO updateEventImage(Long id, EventImageRequestDTO eventImageRequestDTO);
    boolean delEventImage(Long id);
    EventImageResponseDTO findById(Long id);
}
